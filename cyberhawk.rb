require 'mongo'
require 'string'
require 'classifier'

# find our collection
access = Mongo::Connection.new.db("apache")['access']
# get a tailable cursor, set to fire for every new message
start_count = access.count
cursor = Mongo::Cursor.new(access, :tailable => true, :order => [['$natural', 1]])

cursor.skip(start_count - 1)

sim_ddos_log = []
target_log = []
log_size = 20

log_size.times {
  sim_ddos_log << 0
  target_log << 10
}

sim_ddos_log[0]=1
sim_ddos_log[3]=1
sim_ddos_log[2]=1

c = Classifier.new(0.05)

prev = cursor.next_document['time'].to_i

loop do
  if doc = cursor.next_document

    now = doc['time'].to_i
    sub = (prev - now).abs
    prev = now

    sub = 1 if sub == 0

    target_log.shift
    target_log << sub

    if c.predict(target_log, sim_ddos_log) == 1
      puts "#{doc}".red
    else
      puts "#{doc}".green
    end
  else
    sleep 1
  end
end
