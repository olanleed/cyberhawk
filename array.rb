class Array
  def cumsum
    sum = 0
    self.map{ |x| sum += x }
  end

  def /(value)
    self.map { |x| x = x / value.to_f }
  end
end
