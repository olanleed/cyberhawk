# -*- coding: utf-8 -*-
require 'array'

class KS

  def p_nor(z)  # 正規分布の下側累積確率
    z2 = z * z
    t = p = z * Math::exp(-0.5 * z2) / Math::sqrt(2 * PI)
    3.step(200, 2) do |i|
      prev = p;  t *= z2 / i;  p += t
      if (p == prev); return 0.5 + p; end
    end
    (z > 0) ? 1 : 0
  end

  def q_nor(z)  # 正規分布の上側累積確率
    1 - p_nor(z)
  end

  def q_chi2(df,  chi2)  # 上側累積確率
    if (df & 1) == 1  # 自由度が奇数
      chi = Math::sqrt(chi2)
      if (df == 1); return 2 * q_nor(chi); end
      s = t = chi * Math::exp(-0.5 * chi2) / Math::sqrt(2 * Math::PI)
      3.step(df - 1, 2) do |k|
        t *= chi2 / k;  s += t
      end
      2 * (q_nor(chi) + s)
    elsif       # 自由度が偶数
      s = t = Math::exp(-0.5 * chi2)
      2.step(df - 1, 2) do |k|
        t *= chi2 / k;  s += t
      end
      s
    end
  end

  def d_value(x_cum, y_cum)
    cum = []
    x_cum.each_with_index do | v, i |
      cum << (v - y_cum[i])
    end
    cum.map { |value| value.abs }.max
  end

  def ks_test(x, y)
    x_sum = x.inject(:+)
    y_sum = y.inject(:+)
    x_cum = x.cumsum / x_sum
    y_cum = y.cumsum / y_sum
    #差の絶対値の最大値
    d = d_value(x_cum, y_cum)
    #検定統計量
    chi = 4 * d * d * x_sum * y_sum / (x_sum + y_sum)
    #p値
    p = [1, q_chi2(2, chi) * 2].min
    [d, chi, p]
  end

  def test(x, y)
    if x.size == y.size
      ks_test(x, y)
    else
      raise 'x.size != y.size'
    end
  end

end

if __FILE__ == $0

  ks = KS.new
  x = [1,2,1,3,2,3,3,2,7,11,10,9,13,13,22,17,23,20,17,14,13,5,2,1,1,1]
  y = [0,1,2,2,4,5,6,8,10,7,16,17,17,13,19,13,18,10,4,6,6,5,1,3,0,0]

  d, chi, p = ks.test(x, y)
  puts p < 0.05
end
