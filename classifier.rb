require 'Kolmogorov-Smirnov'

class Classifier

  def initialize(alpha = 0.0)
    @alpha = alpha
    @ks = KS.new
  end

  def predict(x = [], y = [])
    d, chi, p = @ks.test(x, y)
    p > @alpha ? 1 : -1
  end

end

if __FILE__ == $0
  c = Classifier.new(0.05)

  x = [1,2,1,3,2,3,3,2,7,11,10,9,13,13,22,17,23,20,17,14,13,5,2,1,1,1]
  y = [0,1,2,2,4,5,6,8,10,7,16,17,17,13,19,13,18,10,4,6,6,5,1,3,0,0]

  puts x.size
  puts c.predict(x, y)
end
