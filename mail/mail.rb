require 'mail'

Mail.defaults do
  delivery_method :smtp, {
    :address => "mail.ms.dendai.ac.jp",
    :port => 587,
    :user_name => "11fi113@ms.dendai.ac.jp",
    :password => "*******",
    :authentication => :plain
  }
end

mail = Mail.new do
  to '11fi113@ms.dendai.ac.jp'
  from '11fi113@ms.dendai.ac.jp'
  subject 'test mail'
  body 'test body'
end

mail.deliver!
